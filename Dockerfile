# Start with a Node image
FROM node:16.19.0-alpine3.16 as build

# Set the working directory
WORKDIR /app

# Copy the package.json and package-lock.json files
COPY package*.json ./

# Install the dependencies
RUN npm i

# Copy the rest of the application files
COPY . .

# Build the React app
RUN npm run build

# Use an Nginx image as the final image
FROM nginx:1.22.1-alpine

# Copy the generated files from the build stage
COPY --from=build /app/build /usr/share/nginx/html

# Setting Nginx config
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf

# Expose port 80
EXPOSE 80

# Copy .env file and shell script to container
WORKDIR /usr/share/nginx/html
COPY ./env.sh .
COPY .env .

# Add bash
RUN apk add --no-cache bash

# Make our shell script executable
RUN chmod +x env.sh

# Start Nginx server
CMD ["/bin/bash", "-c", "/usr/share/nginx/html/env.sh && nginx -g \"daemon off;\""]